import SignInScreen from './pages/auth/signin';

function App() {
  return (
    <div>
      <SignInScreen />
    </div>
  );
}

export default App;
